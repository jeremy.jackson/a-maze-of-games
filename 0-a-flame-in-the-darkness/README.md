# TURN THE TABLES
I'm going to start this off by photo copying the tumblers and making
a papercraft version of the dials. This should help me brute force a
possible solution.

# Solution
> THE GATEKEEPER
>
> WELCOMES ALL TO
>
> THIS LABYRINTH
>
> MOST HAZARDOUS
>
> AND PERPLEXING.
>
> CALIBRATE YOUR
>
> THOUGHTS AS YOU
>
> NAVIGATE PATHS
>
> WITHIN THE MIND,
>
> BUT NOW WE BEGIN.
>
> TO LIFT THE BARS,
>
> ONE PERSON MUST
>
> FIRST TAKE A BOW.

# Photos of Things
![The tumbler I created.](./created-tumbler.jpg)

![The tumbler with the solution.](./tumbler-with-solution.jpg)

![Me bowing.](./me-bowing.jpg)
