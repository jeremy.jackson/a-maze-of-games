# The Pathways of Your Mind
I made a plan for my approach to working through this chapter absed on the
maze map.

I'm going to follow this order:

* 3
* Q
* 8
* 9
* J
* 4
* 7
* 2
* 10
* 5
* A
* 6
* K

![Here's a picture of my plan.](./maze-plan.jpg)
